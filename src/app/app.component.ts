import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  secretDisplayed: boolean = false
  buttonClicks: Date[] = []

  buttonClick(): void {
    this.secretDisplayed = !this.secretDisplayed
    this.buttonClicks.push(new Date())
  }
}
